import setuptools

setuptools.setup(
    name="mobilizon_py_client",
    version="0.0.1",
    author="Marc-AntoineA",
    description="Set of functions to manipulate Mobilizon",
    license="MIT",
    # install_requires=requirements,
    packages=['mobilizon_client'],
    zip_safe=False,
)
