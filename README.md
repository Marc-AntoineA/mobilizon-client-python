# Mobilizon client

## Requirements

```
python3 -m pip install -r requirements.txt
```

## Usage

### CLI

Use `cli.py` entrypoint.

### Library

See `mobilizon_client` for the `GraphQL` api and `mobiliizon_ctl_client.py` for the commands.

## `mobilizon_ctl_client`

This script allows to create users remotely if you have an ssh acccess to your server.
Secrets should be saved in `.env`

## `mobilizon_client`

This script allows to call the GraphQL API. Currently support:
* search for a group
* invite a profile to a group
* upgrade this profile to a specific role (eg. ADMINISTRATOR)