import argparse
import dotenv
import os
import sys
import shutil
import json
import requests
from dotenv import load_dotenv

from mobilizon_client import MobilizonClient
from mobilizon_ctl_client import MobilizonCtlClient

def get_authenticated_client():
    client = MobilizonClient(
                os.environ.get('MOBILIZON_USERNAME'),
                os.environ.get('MOBILIZON_PASSWORD')
            )
    return client

def get_ctl_client():
    load_dotenv('.env')
    ctl_client = MobilizonCtlClient(
        hostname = os.environ.get('MOBILIZON_CTL_URL'),
        port=os.environ.get('MOBILIZON_CTL_PORT'),
        username=os.environ.get('MOBILIZON_CTL_USERNAME'),
        password=os.environ.get('MOBILIZON_CTL_PASSWORD')
        )
    return ctl_client


def handle_groups_create(args):
    client = get_authenticated_client()
    client.create_group(group_id=args.id, name=args.name)

    print(f'''
Le groupe Mobilizon { args.name } a été créé avec succès.,
Pour y accéder : { os.environ.get('MOBILIZON_URL') }/@{ args.id }.
    ''')

def download_image(path, url):
    res = requests.get(url, stream=True)

    if res.status_code == 200:
        with open(path, 'wb') as f:
            shutil.copyfileobj(res.raw, f)

def handle_users_create(args):
    ctl_client = get_ctl_client()
    email, password, display_name = ctl_client.create_member(args.email, args.username)

    print(f'''
:wave: Bonjour @{ display_name },
Tu peux te rendre sur { os.environ.get('MOBILIZON_URL') } et te connecter avec tes identifiants suivants :
* email : `{ email }` ;
* mot de passe : `{ password }`
    ''')

def download_image(path, url):
    res = requests.get(url, stream=True)

    if res.status_code == 200:
        with open(path, 'wb') as f:
            shutil.copyfileobj(res.raw, f)

def handle_groups_events(args):
    path = args.dump_directory
    if os.path.exists(path):
        shutil.rmtree(path)

    os.mkdir(path)
    os.mkdir(os.path.join(path, 'pictures'))

    client = MobilizonClient()
    events = []

    for group_id in args.group_id:
        print(f'Query events: { group_id}')
        events.extend(client.group_oncoming_events(group_id))

    with open(os.path.join(path, 'events.json'), 'w') as f:
        json.dump(events, f, sort_keys=True, indent=4, ensure_ascii=False)

    for event in events:
        print(f'> Download image { event["id"] }')
        if not 'picture' in event.keys(): continue
        if not 'url' in event['picture'].keys(): continue
        url = event['picture']['url']
        extension = os.path.splitext(url)[1]
        download_image(os.path.join(path, 'pictures', event['id'] + extension), url)


def handle_groups_members(args):
    client = get_authenticated_client()

    groups = client.search_groups(args.group_id)
    if len(groups) == 0:
        print(f'No group "{ args.group_id} found, maybe try another key word?')
        return

    if len(groups) == 1:
        group = groups[0]
    else:
        print('We found all these groups ')
        for k, group in enumerate(groups):
            print(f'{ k }. { group["name"]}')

        index = input(f'Please write the index of the group you want to join, between 0 and { len(groups) -1 }: ')
        group = groups[int(index)]

    group_id = group['id']
    members = group['members']['elements']

    member_id = None
    for member in members:
        if member['actor']['preferredUsername'] == args.member_username:
            member_id = member['id']
            break
    else:
        member_id = client.invite_member(group_id, args.member_username)

    client.update_member(member_id, args.role)
    print(f'User @{ args.member_username } has role { args.role } for group { group["name"] }')

dotenv.load_dotenv('.env')
if __name__ == '__main__':


    parser = argparse.ArgumentParser(description='Manage Mobilizon')
    subparsers = parser.add_subparsers(help='sub-command help')

    # USERS
    parser_users_create = subparsers.add_parser('users.create', help='Create a mobilizon user')
    parser_users_create.add_argument('--email', type=str, required=True)
    parser_users_create.add_argument('--username', type=str, required=True)
    parser_users_create.set_defaults(func=handle_users_create)

    # GROUPS
    parser_groups_create = subparsers.add_parser('groups.create', help='Create a Mobilizon Group')
    parser_groups_create.add_argument('--id', type=str, required=True)
    parser_groups_create.add_argument('--name', type=str, required=True)
    parser_groups_create.set_defaults(func=handle_groups_create)

    parser_groups_events = subparsers.add_parser('groups.events', help='List all group events')
    parser_groups_events.add_argument('--group_id', type=str, nargs='+', required=True)
    parser_groups_events.add_argument('--dump_directory', type=str, required=True)
    parser_groups_events.set_defaults(func=handle_groups_events)

    parser_groups_members = subparsers.add_parser('groups.members', help='Manage group admins')
    parser_groups_members.add_argument('--group_id', type=str, required=True, help='Starts with @')
    parser_groups_members.add_argument('--member_username', type=str, required=True, help='Starts with @')
    parser_groups_members.add_argument('--role', type=str, required=False, default='ADMINISTRATOR')
    parser_groups_members.set_defaults(func=handle_groups_members)


    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)
    args = parser.parse_args()
    args.func(args)
