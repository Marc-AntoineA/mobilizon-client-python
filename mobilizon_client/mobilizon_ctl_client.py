from paramiko import SSHClient, AutoAddPolicy

class MobilizonCtlClient:

    _client: SSHClient

    def __init__(self, hostname, port, username, password):
        self._client = SSHClient()
        self._client.load_system_host_keys()
        self._client.set_missing_host_key_policy(AutoAddPolicy)
        self._client.connect(
            hostname=hostname,
            port=port,
            username=username,
            password=password)


    def create_member(self, email, username):
        '''
        Create a new member and return its password
        '''
        command = f'mobilizon_ctl users.new "{ email }" --profile-username "{ username }"'
        stdin, stdout, stderr = self._client.exec_command(command)
        stdout.channel.recv_exit_status()
        lines = stdout.readlines()
        email = lines[1].strip().split(': ')[-1]
        password = lines[2].strip().split(': ')[-1]
        display_name = lines[6].strip().split(': ')[-1]
        return email, password, display_name

