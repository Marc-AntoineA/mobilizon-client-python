
import os
from typing import Dict, Optional
from datetime import datetime, timezone
import requests

class MobilizonClient:

    _api_end_point: str

    def __init__(self, email: Optional[str]=None, password: Optional[str]=None):

        self._api_end_point = os.environ.get('MOBILIZON_URL') + '/api'
        self._token = None
        if email is not None and password is not None:
            self._login(email, password)

    def _request(self, body: Dict, data: Dict):

        headers = {}
        if self._token is not None:
            headers['Authorization'] = f'Bearer { self._token }'

        response = requests.post(url=self._api_end_point, json={ "query": body, "variables": data }, headers=headers)

        if response.status_code == 200:
            response_json = response.json()
            if 'errors' in response_json:
                raise Exception(f'Errors while requesting { body }. { str(response_json["errors"]) }')

            return response_json['data']
        else:
            raise Exception(f'Error while requesting. Status code: { response.status_code }')

    def _login(self, email: str, password: str):
        query = '''
mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
        accessToken
        refreshToken
        user {
        id
        email
        role
        }
    }
}
        '''
        data = {
            "email": email,
            "password": password
        }
        response = self._request(query, data)
        self._token = response['login']['accessToken']

    def invite_member(self, group_id, member_username) -> str:
        '''
        Invite member_username to group_id and return the invitation_id
        '''

        query = '''
mutation InviteMember($groupId: ID!, $targetActorUsername: String!) {
    inviteMember(groupId: $groupId, targetActorUsername: $targetActorUsername) {
        id
    }
}
        '''
        data = {
            'groupId': group_id,
            'targetActorUsername': member_username
        }
        r = self._request(query, data)
        return r['inviteMember']['id']

    def search_groups(self, group_name):
        query = '''
query Groups($groupName: String!) {
    groups(name: $groupName) {
        elements {
            id,
            name,
            members {elements {id, role, actor { preferredUsername }}}
        }
    }
}
        '''
        data = {
            'groupName': group_name
        }
        groups = self._request(query, data)['groups']['elements']
        return groups

    def update_member(self, member_id, role):
        '''
        Change the role of a group member (INVITED, MEMBRE, MODERATOR, ADMINISTRATOR)
        '''
        query = '''
mutation UpdateMember($memberId: ID!, $role: MemberRoleEnum!) {
    updateMember(memberId: $memberId, role: $role) {
        id
    }
}
        '''
        data = {
            'memberId': member_id,
            'role': role,
        }
        r = self._request(query, data)
        return r

    def create_group(self, group_id, name):
        '''
        Create a group "name" with id "@{group_id}". Return internal group id
        '''

        query = '''
mutation createGroup($preferredUsername: String!, $name: String) {
    createGroup(preferredUsername: $preferredUsername, name: $name) {
        id
    }
}
        '''

        data = {
            'preferredUsername': group_id,
            'name': name,
        }
        r = self._request(query, data)
        return r['createGroup']['id']

    def group_oncoming_events_number(self, group_id) -> int:

        query = '''
query($preferredUsername: String!, $afterDatetime: DateTime) {
    group(preferredUsername: $preferredUsername) {
        organizedEvents(afterDatetime: $afterDatetime) {
            total,
        }
    }
}
        '''
        today = datetime.now(timezone.utc).isoformat()
        data = {
            'preferredUsername': group_id,
            'afterDatetime': today
        }
        r = self._request(query, data)
        return r['group']['organizedEvents']['total']

    def group_oncoming_events(self, group_id):
        def _group_oncoming_events_page(page):
            query = '''
query($preferredUsername: String!, $afterDatetime: DateTime, $page: Int) {
    group(preferredUsername: $preferredUsername) {
        organizedEvents(afterDatetime: $afterDatetime, page: $page) {
            elements {
                id,
                title,
                url,
                beginsOn,
                endsOn,
                options {
                showStartTime,
                showEndTime,
                timezone
                },
                attributedTo {
                avatar {
                    url,
                }
                name,
                preferredUsername,
                },
                description,
                onlineAddress,
                physicalAddress {
                locality,
                description,
                region
                },
                tags {
                title,
                id,
                slug
                },
                picture {
                url
                },
                status
            }
        }
    }
}
        '''

            today = datetime.now(timezone.utc).isoformat()
            data = {
                'preferredUsername': group_id,
                'afterDatetime': today,
                'page': page
            }
            r = self._request(query, data)
            return r['group']['organizedEvents']['elements']

        number_events = self.group_oncoming_events_number(group_id)

        events = []
        page = 1
        while len(events) < number_events:
            events.extend(_group_oncoming_events_page(page))
            page += 1
        return events
